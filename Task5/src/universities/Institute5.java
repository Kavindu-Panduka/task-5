package universities;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;

public class Institute5 {
  public static void main(String[] args) {
    List<University> table = Arrays.asList(
    		new University(1,"CINEC campus",200,5000,300,5,10,20,100,1000,50,500,200,40),
            new University(2,"SLIIT",150,4000,250,4,8,15,80,800,40,400,150,30),
            new University(3,"ICBT",100,3000,200,3,5,12,60,600,30,300,100,20),
            new University(4,"UOM",180,4500,280,6,9,18,90,900,45,400,180,35),
            new University(5,"KDU",220,5500,350,7,12,25,120,1200,60,600,250,50),
            new University(6,"UOP",130,3500,220,4,7,14,70,700,35,350,130,25),
            new University(7,"NIBM",90,2500,180,3,4,10,50,500,25,250,80,15),
            new University(8,"SLTC",160,4000,240,5,8,16,80,800,40,400,160,30),
            new University(9,"UOC",140,3200,210,4,6,13,65,650,32,320,120,22),
            new University(10,"ACBT",250,6000,400,8,15,30,150,1500,75,750,300,60),
            new University(11,"Horizon Campus",80,2000,150,2,3,8,40,400,20,200,70,10),
            new University(12,"UOJ",190,4800,290,6,10,22,110,1100,55,550,200,45));

    System.out.println("Sorted by Comparator in Club class");
    table.stream().sorted().forEach(System.out::println);

    System.out.println();
    System.out.println("Sorted by lambda");
    table.stream()
         .sorted((c1, c2) -> 
            ((Integer) c1.getEventsPerYear()).compareTo(c2.getEventsPerYear()))
         .forEach(System.out::println);

  }

}
