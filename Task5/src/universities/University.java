package universities;

/******************************************************************************
 * This class stores the details of a Premiership rugby club including the 
 * performance measures that determine their position in the league. The class 
 * implements the Comparable interface, which determines how clubs will be 
 * sorted.     
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class University implements Comparable<University> {
  private int ranking;
  private String uName;
  private int courses;
  private int students;
  private int lectureres;
  private int canteens;
  private int libraries;
  private int laboratories;
  private int supportingStaff;
  private int graduatesPerYear;
  private int eventsPerYear;
  private int avaliableComputers;
  private int parkingLots;
  private int buildings;

  public University(int ranking, String uName, int courses, int students, int lecturers,
      int canteens, int libraries, int laboratories, int supportingStaff,
      int graduatesPerYear, int eventsPerYear, int avaliableComputers, int parkingLots,
      int buildings) {
    this.ranking = ranking;
    this.uName = uName;
    this.courses = courses;
    this.students = students;
    this.lectureres = lecturers;
    this.canteens = canteens;
    this.libraries = libraries;
    this.laboratories = laboratories;
    this.supportingStaff = supportingStaff;
    this.graduatesPerYear = graduatesPerYear;
    this.eventsPerYear = eventsPerYear;
    this.avaliableComputers = avaliableComputers;
    this.parkingLots = parkingLots;
    this.buildings = buildings;
  }

  public String toString() {
    return String.format("%-3d%-20s%10d%10d%10d", ranking, uName, libraries,
        laboratories, buildings);
  }

  public int getRanking() {
    return ranking;
  }

  public void setRanking(int ranking) {
    this.ranking = ranking;
  }

  public String getUniversity() {
    return uName;
  }

  public void setUiversity(String university) {
    this.uName = university;
  }

  public int getCourses() {
    return courses;
  }

  public void setCourses(int courses) {
    this.courses = courses;
  }

  public int getStudents() {
    return students;
  }

  public void setStudents(int students) {
    this.students = students;
  }

  public int getLecturers() {
    return lectureres;
  }

  public void setlecturers(int lecturers) {
    this.lectureres = lecturers;
  }

  public int getCanteens() {
    return canteens;
  }

  public void setCanteens(int canteens) {
    this.canteens = canteens;
  }

  public int getLibraries() {
    return libraries;
  }

  public void setLibraries(int libraries) {
    this.libraries = libraries;
  }

  public int getLaboratories() {
    return laboratories;
  }

  public void setLaboratories(int laboratories) {
    this.laboratories = laboratories;
  }

  public int getSupprotingStaff() {
    return supportingStaff;
  }

  public void setSupportingStaff(int supportingStaff) {
    this.supportingStaff = supportingStaff;
  }

  public int getGraduatesPerYear() {
    return graduatesPerYear;
  }

  public void setGraduatesPerYear(int graduatesPerYear) {
    this.graduatesPerYear = graduatesPerYear;
  }

  public int getEventsPerYear() {
    return eventsPerYear;
  }

  public void setEventsPerYear(int eventsPerYear) {
    this.eventsPerYear = eventsPerYear;
  }

  public int getAvaliableComputers() {
    return avaliableComputers;
  }

  public void SetAvaliableComputers(int avaliableComputers) {
    this.avaliableComputers = avaliableComputers;
  }

  public int getParkingLots() {
    return parkingLots;
  }

  public void setParkingLots(int parkingLots) {
    this.parkingLots = parkingLots;
  }

  public int getBuildings() {
    return buildings;
  }

  public void setBuildings(int buildings) {
    this.buildings = buildings;
  }
  
  public int compareTo(University c) {
    return ((Integer) ranking).compareTo(c.ranking);
  }
}
