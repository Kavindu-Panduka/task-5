package files;

import java.io.*;

/******************************************************************************
 * This program reads a text file and only prints the lines containing the word
 * "his".
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Files03 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("C:\\\\Users\\\\KAVINDU\\\\git\\\\task-5\\\\Task5\\\\src\\\\data\\\\university.txt"));

    r.lines().filter(l -> l.contains("and"))
        .forEach(l -> System.out.println(l));

    r.close();
  }

}
