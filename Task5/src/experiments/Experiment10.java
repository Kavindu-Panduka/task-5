package experiments;

import java.util.*;
import java.util.function.Predicate;

/******************************************************************************
 * This version introduces uses a predicate for the same task more explicitly.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

	public class Experiment10 {
	  class EFilter implements Predicate<String>{
	    public boolean test(String university) {
	      return university.contains("B");
	    }    
	  }
	
	  public void run() {
	    String[] n1 = {"CINEC", "SLIIT", "ICBT", "NIBM", "KDU", "ACBT", "APIIT", "SLTC"};
	
	    List<String> n2 = Arrays.asList(n1);
	
	    n2.stream().filter(new EFilter())
	        .forEach(university -> System.out.println(university));
	
	  }
	
	  public static void main(String[] args) {
	    new Experiment10().run();
	  }
	}
