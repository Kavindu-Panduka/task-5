package experiments;

import java.util.*;

/******************************************************************************
 * This version introduces the use of a Function. A function takes a parameter
 * and returns a result. In this case all occurrences of the lowercase letter e 
 * are removed from the names. 
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment11 {

  public void run() {
    String[] n1 = {"CINEC", "SLIIT", "ICBT", "NIBM", "KDU", "ACBT", "APIIT", "SLTC"};

    List<String> n2 = Arrays.asList(n1);

    n2.stream().map(university -> university.replaceAll("B", ""))
        .forEach(university -> System.out.println(university));

  }

  public static void main(String[] args) {
    new Experiment11().run();
  }
}
