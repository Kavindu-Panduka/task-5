package experiments;

import java.util.stream.Stream;

/******************************************************************************
 * This version uses a StreamBuilder to generate a Stream of Names.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment14 {
  class UNiversitySreamBuilder {
    public Stream<String> build(){
      Stream.Builder<String> builder = Stream.builder(); 
      builder.add("CINEC");
      builder.add("SLIIT");
      builder.add("ICBT");
      builder.add("NIBM");
      builder.add("KDU");
      builder.add("ACBT");
      builder.add("APIIT");
      builder.add("SLIIT");
      return builder.build();
    }
  }
  
  public void run() {
    UNiversitySreamBuilder builder = new UNiversitySreamBuilder();
    Stream <String> universityStream = builder.build();
    universityStream.forEach(n -> System.out.println(n));
  }

  public static void main(String[] args) {
    new Experiment14().run();
  }
}
