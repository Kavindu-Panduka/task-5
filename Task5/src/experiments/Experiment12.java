package experiments;

import java.util.*;
import java.util.function.Function;

/******************************************************************************
 * This version does the same as the last except it is more explicit about the
 * Function implementation.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class Experiment12 {
  class BRemover implements Function<String, String> {
    public String apply(String univeristy) {
      return univeristy.replaceAll("e", "");
    }
  }
  
  public void run() {
    String[] n1 = {"CINEC", "SLIIT", "ICBT", "NIBM", "KDU", "ACBT", "APIIT", "SLTC"};

    List<String> n2 = Arrays.asList(n1);

    n2.stream().map(new BRemover())
        .forEach(university -> System.out.println(university));

  }

  public static void main(String[] args) {
    new Experiment12().run();
  }
}
